<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NouveauSuiveurMail extends Mailable
{
    use Queueable, SerializesModels;

    public $suiveur;

    public function __construct($suiveur)
    {
        $this->suiveur = $suiveur;
    }

    public function build()
    {
        return $this->subject('Vous avez un nouveau suiveur !')
            ->markdown('mails.nouveau_suiveur');
    }
}
